package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String AMOUNT_OVER = "合計金額が10桁を超えました";
	private static final String NOT_BRANCH_CORD  = "の支店コードが不正です";
	private static final String NOT_COMMODITY_CORD  = "の商品コードが不正です";
	private static final String NOT_RCD_FORMAT  = "のフォーマットが不正です";
	private static final String NOT_RCD_SERIAL_NUMBER  = "売り上げファイル名が連番になっていません";



	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */

	public static void main(String[] args) {

		// コマンドライン引数の確認
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();

		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();

		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales,"[0-9]{3}", "支店")) {
			return;
		}

		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "^[A-Za-z0-9]{8}", "商品")) {
			return;
		}


		// 売上ファイルを検索し保持
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for(int i = 0; i < files.length; i++){
			String filename= files[i].getName().substring(0,8);
			if(filename.matches("[0-9]{8}")) {
				rcdFiles.add(files[i]);
			}
		}

		//連番か確認
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i+1).getName().substring(0,8));
			if((latter - former) != 1) {
				System.out.println(NOT_RCD_SERIAL_NUMBER);
				return;
			}
		}

		for(int i =0; i < rcdFiles.size(); i++) {
			List<String> rcdvalues = new ArrayList<>();
			// 売上ファイル読み込み処理
			if(!readRcdFile(rcdFiles.get(i), rcdvalues)) {
				return;
			}

			//売上ファイルのフォーマットの確認
			if(rcdvalues.size() != 3) {
				System.out.println(rcdFiles.get(i).getName() + NOT_RCD_FORMAT);
				return;
			}

			// 支店コードの確認
			if(!branchSales.containsKey(rcdvalues.get(0))) {
				System.out.println(rcdFiles.get(i).getName() + NOT_BRANCH_CORD );
				return;
			}

			// 商品コードの確認
			if(!commoditySales.containsKey(rcdvalues.get(1))) {
				System.out.println(rcdFiles.get(i).getName() + NOT_COMMODITY_CORD );
				return;
			}


			// 売上額が数字か確認
			if(!rcdvalues.get(2).matches("^[0-9]+$")) {
				System.out.println(UNKNOWN_ERROR);
				return;
			}

			// 集計
			long fileSale = Long.parseLong(rcdvalues.get(2));
			// 支店別売上額の集計
			Long branchSaleAmount =  branchSales.get(rcdvalues.get(0)) + fileSale;
			// 商品別売上額の集計
			Long commoditySaleAmount =  commoditySales.get(rcdvalues.get(1)) + fileSale;
			//11桁を超えている場合
			if(branchSaleAmount >= 10000000000L || commoditySaleAmount >= 10000000000L) {
				System.out.println(AMOUNT_OVER);
				return;
			}
			branchSales.replace(rcdvalues.get(0), branchSaleAmount);
			commoditySales.replace(rcdvalues.get(1), commoditySaleAmount);
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}



	/**
	 * 定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードまたは商品コードと支店名または商品名を保持するMap
	 * @param 支店コードまたは商品コードと売上金額を保持するMap
	 * @param 正規表現の条件
	 * @param エラーメッセージで使用する各定義ファイルの名前
	 * @return 読み込み可否
	 */

	private static boolean readFile(String path, String fileName,
			Map<String, String> names, Map<String, Long> sales,String match, String errorMessage) {
		BufferedReader br = null;
		try {
			File file = new File(path, fileName);
			// ファイルが存在するか確認
			if(!file.exists()) {
				System.out.println(errorMessage + FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;

			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				//分割
				String[] items = line.split(",");
				//フォーマットの確認
				if((items.length !=2) || (!items[0].matches(match))) {
					System.out.println(errorMessage + FILE_INVALID_FORMAT);
					return false;
				}

				// 0番目が支店番号または商品番号,1番目が支店名または商品名
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}



	/**
	 *
	 * 売り上げファイル読み込み処理
	 *@param フォルダパス
	 *@param 売り上げファイルの中身を保持するlist
	 *@return 読み込み可否
	 *
	 */

	private static boolean readRcdFile(File rcdFile,List<String>rcdvalues) {
		BufferedReader br = null;
		try {
			FileReader fr = new FileReader(rcdFile);
			br = new BufferedReader(fr);
			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				rcdvalues.add(line);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				}catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}



	/**
	 * 集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードまたは商品コードと支店名または商品名を保持したMap
	 * @param 支店コードまたは商品コードと売上金額を保持したMap
	 * @return 書き込み可否
	 */

	private static boolean writeFile(String path, String fileName,
			Map<String, String> Names, Map<String, Long> Sales) {

		BufferedWriter bw = null;
		// 支店別集計ファイル作成
		try {
			File file = new File(path, fileName);
			bw = new BufferedWriter(new FileWriter(file));
			// keyを取得し書き込み
			for(String key: Names.keySet()) {
				String write = key + "," + Names.get(key) + "," + Sales.get(key);
				bw.write(write);
				bw.newLine();
			}

		}catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
